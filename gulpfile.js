var gulp = require('gulp');
var gutil = require('gulp-util');
var watch = require('gulp-watch');
var plumber = require('gulp-plumber');
var concat = require('gulp-concat');
var sass = require('gulp-sass');
var autoprefixer = require('gulp-autoprefixer');
var minifyCss = require('gulp-minify-css');
var rename = require('gulp-rename');
var babelify = require('babelify');
var browserify = require('browserify');
var buffer = require('vinyl-buffer');
var source = require('vinyl-source-stream');
var vinylPaths = require('vinyl-paths');
var uglify = require('gulp-uglify');
var ngAnnotate = require('gulp-ng-annotate');
var sourcemaps = require('gulp-sourcemaps');
var notify = require('gulp-notify');
var browserSync = require('browser-sync');
var del = require('del');

var paths = {
  scss: [
    './src/scss/*.scss', 
    './src/scss/**/*.scss', 
  ],
  js: [
    './src/app/*.js', 
    './src/app/**/*.js', 
  ],
  html: [
    './src/app/**/*.html',  
  ],
  other: [
    './src/img/**/*', 
    './src/fonts/**/*'
  ]
};

// error function for plumber
var onError = function (err) {
  gutil.beep();
  console.log(err);
  this.emit('end');
};

gulp.task('default', ['watch']);
gulp.task('build', ['index', 'scss', 'js', 'html', 'img', 'fonts']);

gulp.task('scss', function(done) {
  return gulp.src('./src/scss/styles.scss')
    .pipe(plumber({ errorHandler: onError }))
    .pipe(sourcemaps.init())
    .pipe(sass({ 
      errLogToConsole: true,
      style: (process.env.NODE_ENV == 'production' ? 'compressed' : 'expanded')
    }).on('error', sass.logError))
    .pipe(sourcemaps.write('./'))
    .pipe(gulp.dest('./dist/css/'))
    .pipe(notify({ message: 'SCSS task complete' }))
});

gulp.task('js', function(done) {  

  var bundler = browserify({
    entries: './src/app/index.js',
    debug: true
  });

  bundler.transform('babelify', {
    presets: ['es2015']
  });

  return bundler.bundle()
    .pipe(plumber({ errorHandler: onError }))
    .pipe(source('bundle.js'))
    .pipe(ngAnnotate())
    .pipe(buffer())
    .pipe(sourcemaps.init({ loadMaps: true }))
    .pipe(uglify())
    .pipe(sourcemaps.write('./'))
    .pipe(gulp.dest('./dist/app'))
    .pipe(notify({ message: 'JS task complete' }));

});

gulp.task('html', function(done) {
  return gulp.src(paths.html)
    .pipe(gulp.dest('./dist/app'))
    .pipe(notify({ message: 'HTML task complete' }))
});

gulp.task('img', function(done) {
  return gulp.src(['./src/assets/img/*', './src/assets/img/**/*'])
    .pipe(gulp.dest('./dist/assets/img'))
});

gulp.task('fonts', function(done) {
  return gulp.src(['./src/assets/fonts/*', './src/assets/fonts/**/*'])
    .pipe(gulp.dest('./dist/assets/fonts'))
});

gulp.task('clean', function() {
  return gulp.src('./dist/*')
    .pipe(vinylPaths(del))
    .pipe(gulp.dest('./dist'))
});

gulp.task('browser-sync', function() {
  browserSync({
    server: {
      baseDir: './dist'
    },
    port: 8080,
    open: false,
    ghostMode: false
  });
});

gulp.task('reload', function() {
  browserSync.reload();
});

gulp.task('index', function() {
  return gulp.src('./src/index.html')
    .pipe(gulp.dest('./dist'));
});

gulp.task('watch', ['browser-sync'], function() {
  
  gulp.watch(paths.scss, ['scss'])
  gulp.watch(paths.js, ['js']);
  gulp.watch(paths.html, ['html']);
  gulp.watch(paths.other, ['img', 'fonts']);
  gulp.watch('./src/index.html', ['index']);

  gulp.watch([
    'dist/css/*.css', 
    'dist/**/*.js',
    'dist/index.html'
  ], ['reload'])
});
