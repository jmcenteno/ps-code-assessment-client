import * as Constants from './constants';
import { RoutesConfig } from './routes';
import { InterceptorsConfig } from './interceptors';

export default {
	RoutesConfig,
	InterceptorsConfig,
	Constants
}
