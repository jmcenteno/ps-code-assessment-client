/**
 * Application Routes
 */
function RoutesConfig ($stateProvider, $urlRouterProvider) {
  'ngInject';

  $stateProvider

    // base route
    .state('app', {
      url: '/app',
      abstract: true,
      templateUrl: 'app/shared/templates/layouts/main.html',
      controller: 'MainCtrl as $ctrl'
    })

    // dashboard route
    .state('app.dashboard', {
      url: '/dashboard',
      views: {
        main: {
          templateUrl: 'app/modules/dashboard/views/home/template.html',
          controller: 'DashboardCtrl as $ctrl'
        }
      },

      // options for breadcrumb module
      ncyBreadcrumb: {
        label: 'Customers'
      }

    })

    // customer list route
    .state('app.customers', {
      url: '/customers',
      views: {
        main: {
          templateUrl: 'app/modules/customers/views/search/template.html',
          controller: 'CustomerSearchCtrl as $ctrl'
        }
      },

      // options for breadcrumb module
      ncyBreadcrumb: {
        label: 'Customers'
      }

    })

    // customer details route
    .state('app.customerDetails', {
      url: '/customers/:id',
      views: {
        main: {
          templateUrl: 'app/modules/customers/views/details/template.html',
          controller: 'CustomerDetailsCtrl as $ctrl'
        }
      },

      // options for breadcrumb module
      ncyBreadcrumb: {
        parent: 'app.customers', 
        label: '{{ $ctrl.customer.models.firstName + " " + $ctrl.customer.models.lastName }}'
      },

      // get data before entering the view
      resolve: {
        customerDetails: function (CustomersFactory, $stateParams, $state) {

          // get the customer by the ID set in the state parameters
          return CustomersFactory.getCustomerById($stateParams.id).then(function (data) {
            
            return data;

          }, function () {

            // redirect to customer list route if something goes wrong with the request
            $state.go('customers');

          });

        },
        titleOptions: function (OptionsFactory) {

          // get the title options
          return OptionsFactory.getTitles().then(function (data) {
            return data;
          });

        }
      }
    });

    // redirect to customers view as a failsafe
    $urlRouterProvider.otherwise('/app/customers');

};

export { RoutesConfig };
