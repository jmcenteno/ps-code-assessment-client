function InterceptorsConfig ($httpProvider) {
  'ngInject';
  
  const requestsInterceptor = function($q, $injector) {
    'ngInject';

    return {

      request: function(config) {      
        return config;
      },

      response: function(response) {
        return response;
      },

      responseError: function(rejection) {
      
        const $timeout = $injector.get('$timeout');
        const $state = $injector.get('$state');
        const toastr = $injector.get('toastr');

        let msg = 'We\'re unable to process your request.';

        switch (rejection.status) {

          case 404 :

            break;

          case 500 : 

            toastr.error(msg, 'Error');

            break;

          default : 

            console.log(rejection.status);
            break;

        }

        return $q.reject(rejection);

      }

    }

  }

  $httpProvider.interceptors.push(requestsInterceptor);

}

export { InterceptorsConfig };
