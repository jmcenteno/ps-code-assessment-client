// load all 3rd party modules
import angular from 'angular';
import uiRouter from 'angular-ui-router';
import ngBreadcrumb from 'angular-breadcrumb';
import ngAnimate from 'angular-animate';
import ngResource from 'angular-resource';
import ngAria from 'angular-aria';
import ngSanitize from 'angular-sanitize';
import uiBootstrap from 'angular-ui-bootstrap';
import toastr from 'angular-toastr';

export default {}
