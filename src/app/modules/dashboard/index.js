/**
 * Dashboard Module
 */
import angular from 'angular';

// import all controllers
import { HomeController } from './views/home/controller';

// define the module
const module = angular.module('ps.dashboard', []);

// define module controllers
module.controller('DashboardCtrl', HomeController);


export default module;
