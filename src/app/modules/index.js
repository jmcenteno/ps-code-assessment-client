import DashboardModule from './dashboard';
import CustomersModule from './customers';

export default {
	DashboardModule,
	CustomersModule
};
