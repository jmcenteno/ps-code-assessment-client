class CustomersService {
	
	constructor ($http, $q, API_ENDPOINT) {
		'ngInject';

		this.endpoint = API_ENDPOINT + '/customers';
		this._$http = $http;
		this._$q = $q;

	}

	getCustomers () {

		let deferred = this._$q.defer();
		
		this._$http.get(this.endpoint)
			.success(function (data) {
				deferred.resolve(data);
			})
			.error(function (error) {
				deferred.reject(error)
			});
		
		return deferred.promise;
		
	}

	getCustomerById (id) {

		let deferred = this._$q.defer();
		
		this._$http.get(this.endpoint + `/${id}`)
			.success(function (data) {
				deferred.resolve(data);
			})
			.error(function (error) {
				deferred.reject(error);
			});

		return deferred.promise;

	}

	updateCustomer (id, values) {

		let deferred = this._$q.defer();

		this._$http.put(this.endpoint + `/${id}`, values)
			.success(function (data) {
				deferred.resolve(data);
			})
			.error(function (error) {
				deferred.reject(error);
			});		

		return deferred.promise;

	}

	deleteCustomer (id) {
		
		let deferred = this._$q.defer();

		this._$http.delete(this.endpoint + `/${id}`)
			.success(function (data) {
				deferred.resolve(data);
			})
			.error(function (error) {
				deferred.reject(error);
			});		

		return deferred.promise;

	}

	static factory ($http, $q, API_ENDPOINT) {
		'ngInject';

		return new CustomersService($http, $q, API_ENDPOINT);

	}

}

export { CustomersService };
