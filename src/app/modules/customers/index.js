/**
 * Customers Module
 */
import angular from 'angular';

// import all controllers
import { CustomerSearchController } from './views/search/controller';
import { CustomerDetailsController } from './views/details/controller';

// import all services
import { CustomersService } from './services/customers';

// import all directives
import { ConfirmationModalDirective } from './directives/confirmation-modal';

// define the module
const module = angular.module('ps.customers', []);

// define module controllers
module.controller('CustomerSearchCtrl', CustomerSearchController);
module.controller('CustomerDetailsCtrl', CustomerDetailsController);

// define module services
module.factory('CustomersFactory', CustomersService.factory);

// define module directives
module.directive('confirmationModal', ConfirmationModalDirective.factory);


export default module;
