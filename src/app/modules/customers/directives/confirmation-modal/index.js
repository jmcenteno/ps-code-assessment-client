/**
 * Confirmation Modal
 * @description Render the markup for a Bootstrap modal with two confirmation actions (Yes/No)
 */
class ConfirmationModalDirective {

	constructor () {

		this.restrict = 'EA';
		this.templateUrl = './app/modules/customers/directives/confirmation-modal/template.html';
		this.replace = true;

	}

	static factory () {

		ConfirmationModalDirective.instance = new ConfirmationModalDirective();
		return ConfirmationModalDirective.instance;

	}

}

export { ConfirmationModalDirective };
