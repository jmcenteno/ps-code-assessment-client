import _ from 'lodash';

function CustomerSearchController ($scope, $rootScope, $state, CustomersFactory, $uibModal, toastr, $timeout) {
	'ngInject';

	const $ctrl = this;

	// this will hold an array of all customers
	let customers = [];

	// set the page title (title tag)
	$rootScope.pageTitle = 'Customer Search';

	// get all customer records
  let getCustomers = function () {

  	$ctrl.gridViewConfig.data = null;

  	CustomersFactory.getCustomers().then(function (data) {
			
			// loop through the collection and change the value of maritalStatus
			customers = _.map(data, (item) => {
				item.maritalStatus = item.maritalStatus.name;
				return item;
			});

			// pass the customers data to the view
			$ctrl.gridViewConfig.data = customers; 

		}, function () {

			// fallback to an empty array if request fails
			$ctrl.gridViewConfig.data = []; 	

		});

  }

	// grid-view configuration
  $ctrl.gridViewConfig = {

  	// table headers
    headers: [
      {
        label: 'First Name',
        field: 'firstName',
        sortable: true
      },
      {
        label: 'Middle Name',
        field: 'middleName',
        sortable: true
      },
      {
        label: 'Last Name',
        field: 'lastName',
        sortable: true
      },
      {
        label: 'Nickname',
        field: 'nickname',
        sortable: true
      },
      {
        label: 'Marital Status',
        field: 'maritalStatus',
        sortable: true
      }
    ],

    data: null, // set to null to show loading animation

    // initial sorting options
    sortType: 'firstName', 
    sortReverse: false,
    scrollable: true,

    // onclick handlers for edit and delete buttons
    actions: {
    	edit: function (id) {

    		// redirect to the customer details view
		 		$state.go('app.customerDetails', { id: id });    

		  },
    	delete: function (id) {

    		// create a new modal instance and set a controller for it
				$uibModal.open({
		      template: '<div data-confirmation-modal=""></div>',
		      controller: function ($scope, $uibModalInstance) {
						'ngInject';

						// pass data to the modal
						$scope.title = 'Confirm';
						$scope.message = 'Are you sure you want to delete this record?';
						$scope.actions = {
							ok: function () {

								// delete the customer
								CustomersFactory.deleteCustomer(id).then(function (data) {

									// wait a second before getting the updated list of customers
									$timeout(function () {
										toastr.success('Customer deleted.');
										getCustomers();
									}, 1000);

								}, function() {
									toastr.error('The customer could not be deleted.', 'Error');
								});

								// close the modal
								$uibModalInstance.close();

							},
							cancel: function () {

								// close the modal
								$uibModalInstance.dismiss('cancel');

							}
						}

					}
		    });

			}
    }
  };

  // configuration object for the search form
	$ctrl.search = {

		// initiate the form models with empty strings
		models: {
			firstName: '',
			lastName: ''
		},

		// form onsubmit handler
		submitHandler: function () {		

			// filter the customers array based on the search parameters
			$ctrl.gridViewConfig.data = _.filter(customers, (item) => {
				if (
					($ctrl.search.models.firstName != '' && item.firstName.toLowerCase().indexOf($ctrl.search.models.firstName.toLowerCase()) != -1) || 
					($ctrl.search.models.lastName != '' && item.lastName.toLowerCase().indexOf($ctrl.search.models.lastName.toLowerCase()) != -1)
				) 
				{
					return true;
				}
			});

		},

		// onreset handler
		clear: function () {

			// set the grid view data back to the original data array
			$ctrl.gridViewConfig.data = customers;

		}
	}

	// get all customers
	getCustomers();

}

export { CustomerSearchController };
