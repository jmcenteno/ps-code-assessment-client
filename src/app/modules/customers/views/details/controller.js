/**
 * Customer Details Controller
 */
function CustomerDetailsController ($scope, $rootScope, $state, CustomersFactory, customerDetails, titleOptions, toastr) {
	'ngInject';

	const $ctrl = this;

	// set the page title
	$rootScope.pageTitle = 'Manage Customer';

	$ctrl.customer = {

		// set form models
		models: angular.copy(customerDetails),

		// onsubmit event handler
		submitHandler: function () {

			// send request to the API to update the current customer
			CustomersFactory.updateCustomer($state.params.id, $ctrl.customer.models).then(function (data) {

				// display a success message
				toastr.success('Customer updated.');

			}, function (error) {

				// display an error message
				toastr.warning(error.message);

			});

		},

		// set form models to their original state
		clear: function () {

			$ctrl.customer.models = angular.copy(customerDetails);

		}
	}

	// title options for dropdown menu
	$ctrl.titles = titleOptions;

}


export { CustomerDetailsController };
