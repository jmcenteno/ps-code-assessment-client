// load 3rd party modules
import vendors from './vendors';

// load app cofiguration
import Config from './config';

// load app modules
import SharedModules from './shared';
import FeatureModules from './modules';

// load bootstraping function
import RunApplication from './run';

// main module definition
const app = angular.module('psCodeAssesment', [
  
  // third party modules
  'ui.router',
  'ngResource',
  'ngAnimate',
  'ngSanitize',
  'ngAria',
  'ui.bootstrap',
  'ncy-angular-breadcrumb',
  'toastr',

  // shared modules
  'ps.controllers',
  'ps.directives',
  'ps.services',
  'ps.filters',

  // modules by feature
  'ps.dashboard',
  'ps.customers'

]);

// define app conttants
for (let key in Config.Constants) {
  app.constant(key, Config.Constants[key]);  
}

// define app configuration
app.config(Config.RoutesConfig)
app.config(Config.InterceptorsConfig)

// bootstrap the appilcation
app.run(RunApplication);


export default app;
