/**
 * Social Securit Number Filter
 * @return {string} Formatted social security number
 */
function SsnFilter () {
	
	function format (value) {
	  
    // make sure that ssn is always a string
    let ssn = (value ? value.toString() : '');

    if (ssn.length > 3) {
      
      // get the first 3 characters and append a dash at the end      
      value = ssn.substr(0, 3) + '-';
      
      if (ssn.length > 5) {

        // get the characters from fourth position to sixth and append a dash at the end
        value += ssn.substr(3, 2) + '-';

        // get the last four character
        value += ssn.substr(5, 4);

      } else {

        value += ssn.substr(3);

      }

    }

    return value;

	}

	return format;

}

export { SsnFilter };
