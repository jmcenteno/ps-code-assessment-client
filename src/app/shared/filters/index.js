/**
 * Shared Filters Module
 */

import angular from 'angular';

// import shared custom filters
import { SsnFilter } from './ssn.js'

// define filters module
const FiltersModule = angular.module('ps.filters', []);

// define all shared filters
FiltersModule.filter('ssn', SsnFilter);

export { FiltersModule };
