/**
 * Grid View Directive
 * @description Renders an HTML table with sorting and pagination
 */
class GridViewDirective {
  
	constructor () {
		'ngInject';

		this.restrict = 'A';
    this.templateUrl = './app/shared/directives/grid-view/template.html';
    this.scope = {
      config: '=gridView'
    };

	}

	controller ($scope) {
    'ngInject';

    // set pagination options
		$scope.itemsPerPageOptions = [10, 25, 50, 100];
    $scope.itemsPerPage = $scope.itemsPerPageOptions[0];
    $scope.currentPage = 0;

	}

	link (scope, element, attrs) {

	}

  static factory () {
    'ngInject';

    GridViewDirective.instance = new GridViewDirective();
    return GridViewDirective.instance;

  }
}

export { GridViewDirective }
