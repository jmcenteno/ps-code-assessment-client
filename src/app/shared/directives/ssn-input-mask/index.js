/**
 * SSN Input Mask
 * @description Apply an input mask to social security numbers
 */

import angular from 'angular';

class SsnInputMaskDirective {

	constructor ($filter) {
		'ngInject';

		this.restrict = 'A';
		this.require = 'ngModel'

		// make services available in other class methods
		this._$filter = $filter;

	}

	link (scope, element, attrs, ngModel) {

		ngModel.$parsers.push((value) => {

      value = value.replace(/[^0-9]/g, '');
      
      // remove additional characters if the length of the number is greater than 9
      if (value.length > 9) {
        value = value.substr(0, 9);
      }
      
      // format the value with the ssn filter
      let newValue = this._$filter('ssn')(value);

      // set the new value in the model and display it
      ngModel.$setViewValue(newValue);
      ngModel.$render();
      
      return newValue;

    });

	}

	static factory ($filter) {
		'ngInject';

		SsnInputMaskDirective.instance = new SsnInputMaskDirective($filter);
		return SsnInputMaskDirective.instance;

	}

}

export { SsnInputMaskDirective };
