/**
 * User Menu Directive
 */
class UserMenuDirective {

	constructor () {

		this.restrict = 'EA';
		this.templateUrl = './app/shared/directives/user-menu/template.html';
		this.replace = true;
		this.scope = {};

	}

	static factory () {

		UserMenuDirective.instance = new UserMenuDirective();
		return UserMenuDirective.instance;

	}

}

export { UserMenuDirective };
