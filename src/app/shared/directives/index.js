/**
 * Shared Directives Module
 */

import angular from 'angular';
import ngUtilsPagination from 'angular-utils-pagination';

// import shared directives
import { MainNavDirective } from './main-navigation';
import { UserMenuDirective } from './user-menu';
import { GridViewDirective } from './grid-view';
import { SpinnerDirective } from './spinner';
import { SsnInputMaskDirective } from './ssn-input-mask';

// define directives module
const DirectivesModule = angular.module('ps.directives', [
	'angularUtils.directives.dirPagination'
]);

// define all directives
DirectivesModule.directive('mainNavigation', MainNavDirective.factory);
DirectivesModule.directive('userMenu', UserMenuDirective.factory);
DirectivesModule.directive('gridView', GridViewDirective.factory);
DirectivesModule.directive('spinner', SpinnerDirective.factory);
DirectivesModule.directive('ssnInputMask', SsnInputMaskDirective.factory);	


export { DirectivesModule };
