/**
 * Shared Controllers Module
 */

import angular from 'angular';

// import shared controller functions
import MainController from './main';

// define controllers module
const ControllersModule = angular.module('ps.controllers', []);

// define all shared controllers
ControllersModule.controller('MainCtrl', MainController);

export { ControllersModule };
