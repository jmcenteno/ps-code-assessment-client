/**
 * Main Controller
 */
function MainCtrl ($window) {
	'ngInject';

	const $ctrl = this;

	// check the window with and 
	$ctrl.isCollapsed = ($window.innerWidth > 768);

	// get the current year
	$ctrl.year = (new Date()).getFullYear();

}

export default MainCtrl;
