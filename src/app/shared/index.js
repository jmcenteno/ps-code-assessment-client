import { ControllersModule } from './controllers';
import { DirectivesModule } from './directives';
import { ServicesModule } from './services';
import { FiltersModule } from './filters'

export default {
	ControllersModule,
	DirectivesModule,
	ServicesModule,
	FiltersModule
}
