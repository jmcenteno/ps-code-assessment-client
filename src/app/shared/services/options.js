/**
 * Options Service
 * @description A collection of methods to get data from the API
 */
class OptionsService {

	constructor ($http, $q, API_ENDPOINT) {
		'ngInject';

		// make services available in other methods
		this.endpoint = API_ENDPOINT + '/options';
		this._$http = $http;
		this._$q = $q;

	}

	getGenders () {

		return {
			M: 'Male',
			F: 'Female'
		};

	}

	getTitles () {

		let deferred = this._$q.defer();

		this._$http.get(this.endpoint + '/titles').success(function (data) {
			deferred.resolve(data);
		}).error(function (error) {
			deferred.reject(error);
		});

		return deferred.promise;

	}

	// TODO - Marital Status options

	static factory ($http, $q, API_ENDPOINT) {
		'ngInject';

		return new OptionsService ($http, $q, API_ENDPOINT);

	}

}

export { OptionsService };
