/**
 * Shared Services Module
 */

import angular from 'angular';

// import shared services
import { OptionsService } from './options';

// define services module
const ServicesModule = angular.module('ps.services', []);

// define all shared services
ServicesModule.factory('OptionsFactory', OptionsService.factory);

export { ServicesModule };
